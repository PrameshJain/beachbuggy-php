<?php
require '/home/admin/web/default.domain/public_html/beachbuggy/api/autoload.php';
include('/home/admin/web/default.domain/public_html/beachbuggy/api/config.php');
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
//include('/home/admin/web/default.domain/public_html/beachbuggy/api/push_client.php');
ParseClient::initialize( $app_id, $rest_api,$master_key);
$data = json_encode(array("action_key"=>3,"alert" => 'Your ride has been Cancelled to pick up '.$user_name.' from '.$destination_address.'!!',"sound" => "default"));

// Push to Channels
/*ParsePush::send(array(
  "channels" => ["driver"],
  "data" => $data
));*/

// Push to Query
$query = ParseInstallation::query();
//$query->equalTo('deviceType', 'android');
$query->equalTo('emailID',$email);
ParsePush::send(array(
  "where" => $query,
  "data" => $data
));


?>
