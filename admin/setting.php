<?php
include('includes/conn.php');
if(!isset($_SESSION['admin_id'])){
	header('Location: login.php');
	exit();
}
include('includes/header.php');
$sql="select * from app_time";
$result = mysqli_query($con,$sql);
$row = mysqli_fetch_array($result);

//print_r($row);
if($_POST)
{
	$app_time_from = $_POST['app_time_from'];
	$app_time_to = $_POST['app_time_to'];
	$in_sql = "Update `app_time` SET `app_time_from` = '$app_time_from',`app_time_to` = '$app_time_to'";
	//echo $in_sql;exit;
	$result = mysqli_query($con,$in_sql);
$delay=0;
header("Refresh: $delay;"); 
}

//print_r($row);


 ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">App Off Time</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
			
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            App Off Time Data Table
							<!--<a  href="page_add.php" class="btn btn-primary btn-xs" style="float:right" >Add Pages</a>-->
                        </div>
						
						
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                              <p style="color:red;font-weight:bold;">Following data shows EST when the app service will be turned off.</p>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Start app down time</th>
											<th>End app down time</th>
											<th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<form method="post">
                                        <tr class="odd gradeX">
											<td><p class="text_hide"><?php echo $row['app_time_from']; ?></p>
											<input class="timepicker-default form-control" type="text" name="app_time_from" id="app_time_from"/></td>
											
											<td><p class="text_hide"><?php echo $row['app_time_to']; ?></p><input class="timepicker-default form-control" type="text" name="app_time_to" id="app_time_to"/></td>
											<td><button type="submit" class="btn btn-success set_time" >Set Time</button></form><button type="submit" class="btn btn-success change_time" >Change Time</button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
      
    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Tables -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ssharunas.github.io/bootstrap-timepicker/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function () { 
            $('.timepicker-default').timepicker();	
			//$('#app_time_to') = $('#time_to').val();
		  //$('#app_time_from') = $('#time_from').val();
        });
    </script>
	<script type="text/javascript">
        $(document).ready(function () { 
            $('#app_time_from').hide();
            $('#app_time_to').hide();
            $('.set_time').hide();
			$(".change_time").click(function(){
			$('#app_time_from').show();
            $('#app_time_to').show();
            $('.text_hide').hide();
            $('.change_time').hide();
            $('.set_time').show();
			
			//$('#app_time_from').focus();
			});
        });
    </script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
    </script>

</body>

</html>
