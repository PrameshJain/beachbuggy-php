<?php
include('includes/conn.php');
if(!isset($_SESSION['admin_id'])){
	header('Location: login.php');
	exit();
}
include('includes/header.php');
$sql="select * from tblUser";
$result = mysqli_query($con,$sql);
//$row = mysqli_fetch_array($result);
if($_GET['reset'])
{
	$sql= "Update `tblUser` SET `accessToken` = '' where pkUserID =".$_GET['reset'];
	if (mysqli_query($con,$sql))
	{
		header('Location: rider.php');
		//die('Error: ' . mysqli_error($con));
	}
}

//print_r($row);


 ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Rider</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
			
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Rider Data Table
							<!--<a  href="page_add.php" class="btn btn-primary btn-xs" style="float:right" >Add Pages</a>-->
                        </div>
						
						
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
											<th>Email ID</th>
											<th>Address</th>
											<th>Phone No</th>
											<th>Live Status</th>
											<th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php
									//print_r($result);
									while($row = mysqli_fetch_array($result)){
                                     if($row['status'] == 'UN')
                                     {
									      $status = "UNRegister";
									 }									 
									 if($row['status'] == 'A')
                                     {
									      $status = "Active";
									 }
									?>
                                        <tr class="odd gradeX">
											<td><?php echo $row['firstName'].' '.$row['lastName'] ; ?></td>
											<td><?php echo $row['emailID']; ?></td>
											<td><?php echo $row['homeAddress'] ; ?></td>
											<td><?php echo $row['phoneNo']; ?></td>
											<td><?php echo $status; ?></td>
											<td>
											<?php if($row['accessToken'] != '')
											{
											?>
											<a class="btn btn-primary btn-xs" href="rider.php?reset=<?php echo $row['pkUserID']; ?>" onclick="return confirm('Are You Sure?');">Reset User</a>
											<?php
											}
											?>
											</td>
                                        </tr>
										<?php } ?>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
      
    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Tables -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
    </script>

</body>

</html>
